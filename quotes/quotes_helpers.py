import os
import json
import random
import subprocess as s
import time
import PySimpleGUI as sg
import sys
from pathlib import Path

micon = b'iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAYAAAA4qEECAAAFw0lEQVR42u2dT4gTVxzHA+KlIAYplILIgFA0KzLisgoLy1D2ptAUavFiSQ8q7V4itOLJBsqCIBIUEbELHkqlUCFQvfQ0tIeelIAHRRAEKUKhdEqlh0LgOd9kXjIzO5PMJPN+75fJG/hCSOa9TD7729/7vd/7V6kwunq3lyxfdV8tXx1fri+RUW5QphXUYVXMFQHbCAB5OaBmlRfU3Vg48P4Prvpq+uoqADtJ3eC7q2UG7Pi6qwFumvAsTtkAu4wAJ/l2Z54B28wBJwG3580Ht+cIcFxt9j48CKu8OYYcjlbqxooX0bqDWLhbQsjhkNDi0OB5JYYcdiW2LsiNBQAcV8NALhvsBYdMA9tAJoAdNHykP+bZZk3cOVsTX5w4KM6sHxTHjxyICO/hM9yDezXAtlWEcCTRxT83lvrgPl47sA3sJKEMyqIOwmjEKrIz0qUCvH5sBG599QPx7cb74uGN3eLxD+8I8agSEd7DZ7gH9w7LHSMF3i2kU0PR48O/fdiCvzy9rw8wDnaSUAZlwxZO5FLaReQulD7kgwu1iAX/urUrCrBbFeJFXYjXLSH+dQfyOoP3U4CjjrCF4zsIYNdncRkeFeSLn+8Vb37bMQL23BHir7si9XpijbVu1IU6CWF7U7kQ1S4jDBk+NmLBccD/dYX4sy3Ey8bgD/DUzuxOUDch7DarUA5+MxEyAPa8EWAAzwE1C2wCn23nAa10ZETGxPjXToQMCy4AcFjSjaCBVByNuHnG+JQ9CMIu2fANfXIYMhq7MQ3dtMJ3yQYSz6DYqh2t1gxLknFyJHxDFCEtuWDA8fBPxtlarZrKmhHrRiDAohG+KbDkuGScrdWqVc+7kJ2SiDUjilAMN8mq8Syq542Mi5uVRxrwk8MfDivG9apJClv6aoIIpJoEuknhNiLh3P8vB6DR8yMELcM9AvfRTAKtNHGEdGbEbaBnJy9CyGH3gWdSnXBKSoMKith5mIWDFctwjhg0nkHmswlyIBbpyInsmUUaQVzw08SgIfk8pCMxwRxiWtCaRQi6EwbtGdDqsnpk/nnBQQ/8NEVyP7Ex1CjixnAwKBAsrlH+ZdvCO40iDO+kWiQNYWqHRZMIOyyjBpFqVn5iF1y1EEIi/YrRGT1d8FE2j3LCSWJSSaXQGZIjNfRJpYhIQaemSVVIJqxg0aGBXMI0qT7Q4cT/tmkFRQqDuPLC69A0BKLEv17QqUNZqiCHXAbxUJZ+0LAk6asjg7MKIRMPzvIAPXa6wSyCH5YDvDHIxNMN+IAeO4FmWmG8EaAx0KtvAg0/0Epg652lxBd0HPbWN+8WBhl1cYHMAjR0b6PYSOT1LztZQZagWSyMl0mnH6/smRl0++v3qJNGmbrgHQ4PI11IEb1G2ftzL9W4gO6QpUkn6Y+rxQ0OyHp0xMvj0qR1Jg+Te3HQJDFaKlcnG8rKomW7ONioixFoi2xwNov2Hz7e1737P88kWQ+XBfuk0w0WGHSH3dLjkoJukE4JywP61tb3U0NGWWagLdJJjll0/qPlPqCTpz6bGjTKog7UxWH3GvJpu1n0++XDQ2sEsDyWjXslZAh1MQDdJJ+InlU/Ne0hrGmFOpi4jaqWpRV5ffW04rIlp7bFQqqjD2agHa2LORcEtKt9QeeCuA6HxRLlkoN2We+fVCLXYbPbeSZNa6srfWCb125mhox7UQZlde5lynJjlDR99enRqd0GymrcVrPKdqufST3EvNLYI6yz37xqXN5jacUR125+l+oy8Bnu0ZzfaM/Ndmxx/X39kDj54crQUj85cy7is/Ea7w1zI/69KKNp2+Ni9pim3GAwDlta9jjhHk2Qi9tgkEPIB7+LRk5GIzK6wHuas3Rq9pQ2m8CaHXfNHtIGsoHNH7I5TMEcD1LO40HMgTfmCKciXAW/I5zMoWTmmL1cIyO9eTpmL2UM0mUO2KmU5eqZo1C1+HBzuK+GGNwcV60JfCkPYH8LiFM28ULWqc0AAAAASUVORK5CYII='

EMOJI_BASE64_HAPPY_IDEA = b'iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAYAAACohjseAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RUVEQTM3QTg3NkRGMTFFQjk1QkZCNjE0QUI5MUYwQzciIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RUVEQTM3QTk3NkRGMTFFQjk1QkZCNjE0QUI5MUYwQzciPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpFRURBMzdBNjc2REYxMUVCOTVCRkI2MTRBQjkxRjBDNyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpFRURBMzdBNzc2REYxMUVCOTVCRkI2MTRBQjkxRjBDNyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjcEIkMAABZPSURBVHjavFoJdBzVlb1V1Zu61VJr321ZQpYsbwEbsDF4ZQlgz4ATsjJJGHLIJJ7MyXLmzCQnyQDJ5EwgOVnIMEASDgSSkEmIQ0hsOICDHcAsxrLxgi0hy9rV2rq19FbdVTX3/+qWtVmSCZny+W51ddWvf/+77737/i/FsixMP1588UUMDAxgoYfBLqb3Iro1TMBkK3QAbpV/85yiACrbgA4keZ2m2eemHxrPKed53ngMKMpJYPsmsxaGUiev1Mzu3qBy7I+vuJHtVeBgv1u2bIFjtg7uuusuCXKhh5uN4y/3AFXlTlQTSI2qosTvR77Pi0C+B+4sB8esQiNw0zRgDMaRHI9jZHQcoaSOfoJuj5po602hk4Nqi9hzNOtx/eUI3Pnliq8ivPlmOKvL5RQZPYPu4f3P7v9t692796EzmgAOHDgwO8CFHJwkeCxcWuHCjY1V2LaoFLVLapSyZY1FyCvIh8utwe/qh881DFUzoHEGFM6qRYsaBj/Z4hwEASIRp1WIqPUscLIZ4aFhnDnSioODMTw9aOJlXjJuptHuuAJFv/nput3uxZ/bgFQZO01DMI2q/Jxtn378549tve2Tz/79I3us44IBjncBzFGq4MNL8/FP267Ehm1XFyvVjXXIW7IKyCoBUhxprAOIHmfjyJNGmrPTOlLsVqpigovrN8mPQGwEl7S345LX3sSuvc/jVHMPHjmTwE+W1GL4Nw8su89d870NMPykzSjvdaf742yhkLN3Y81/f2f4of2HXt8aSyC+YIDCb2qduGxJNu7dfo22cfuHVmLJJVcBuSsJyklTHAM6/wRETvB7dAqICz2yvEDDKrvd8H40/OlZ/NcTv8Mnl1XhSXf1Z3bCwR/UsbQT++yHmOKZpEgyAG/ZuvVfvLXppkTSfGJBAMUkr1Dx6S0N+MFnvlTnW3bN33EUDeQWI0Xwz8Dws+TbsG0lJX2DNJoCk18U/iX+JlH5z5Dfz/VtzhK10o1HUTHwqX8AllZhmeHL/xrcm+0fheU0zoSjwL4wxQdbHI8jhz8X4YpLKz5oBJLzAxTRbIUDuyqvv+rHV399J8y6jTis+9E8msLzA0MYHC9CQvkIdGc2UmS8aAKIACT+Fp9KmqFTAdpnHRKJJc870j04aRqPFYcXUfjMCHxKFMbaMdxR+AYBFAl/S7NDPeeDSprrCtmkO7Fmlb8Qixbgg40qrsm7ZssPy7+/Bz0Mh0FSvYle/1Af2ZB6N168wGM6tbOBTc47scoYt0GI/GPQxxFKUzRmW9BK2hNgKTIBzTm8EhW53urK+1xfeURbQ3DCj7sI6sEgGWGco+K5MVnSAoJ2mb8zI9XS56da1pJWFTTOnDendzqRWIFegyiNEcDF6JnipxG2AcnfCZbWhsHA44rgYFMkoo3CmhNghYpPXnLTivqrGyKoiR+BQzHRO/QCfqYfRZGH9LGizIFxSTMHaeVMU2wywIy3ib8z1ExJCquzAkxOIaoTEcWHcMqHoVQOKsNtwBj9rpCBzSSQFP1ejacB6nbTuzmbYYSGBw9aBersBEvnHHdFMW7/5qYzKFCY9C1aXB/CitidDP86mpuoRgYZNNlngpaNJ+1PPWk3kesEhYX9xKdhTQ1aTodtXKE4hJoR311knpuxw+1ijmVzsZXmkppLeY5GG6NbxLqSyArcyA4ZRY2QTc1MZEr18+E9DIDtY4/uHnn8tq84zgNQ0BO4dN1qrCxoXMMR+uyR6kdw7E0d9z0InGrldZrT1lnS2Uk50w4glqrZv012pCl6bLK2o+3oT4qRlNrFktpNtRWB8LNUEmWFFm7/BHDleqD71H7UFvwYSs6V/C1k+6Po2+Isxpo5K2E8/cRLdz71Elo/654jRFB2Xb9+HbOft8YekBZH51uv4s5v0xfifjjrq6G6PNIeYnAKB5fIK+VcBOCIjcEdpqMKoA7XTIAZEUYQmh5DypONeKBEfvcMdjF/D8MUfWsOUPKht7cb9/wgKJlVVW1g6PTPUbhcsyOPjJ6qTVdrBNH+N/78r98d+WGCeB3qeQD6OebFflxV15DPe6kOLF6pt+DJX3WiZ9QD54rl9gA4w2oyjlhZLc6+/zMI112KpCcHjsQoAi2HUL33QfiCZ2A4s2yLTA5IwjqcuK6NH0PPlbcgnlcuv2cNdaH0qftRvO8x8tZNSwWgLalHsiWFJ38/hM/tAoaC/ShceopUTE++mDCF9PSG8OuH2+4/3WlnUet8QV4xULy4BDXF5UyiCp1AtTDW8hoOHWbgKCqCJcAZDCakz1jVMpz+6N28zkLg9GtwjodpxVxEKpfixG3fQ8Mvvw5/59u0pHOqBWnQ1h1fwODF2+DtOYNA8xvSyrHiRej6+L8hXrIYi574NszQoLzFWVqOjrND6KAKrKdPHj54FDGCEvZz0XlVUtvpDI3e92v60aRjVoBuBZVlJSh1FVBGWC45Oy1vHUcXJ0mtzydVbGtY5I/pcKPuN/+J3DZG2eiYHKTFwae8uRipXg3D5ZXXTZlAAtSz8xA404RF+34OD62mpvR0nxqiJUswmr8IKUH36AjM8VGYBUWIKx40N8dxUS3w8J4i3PDxb2F4oBftrZ2IRKJ4+/iRnmOtR4LzAqx0o7KiiPHbWWiXALFTOPomLePwQHV7J3zIUh0SmJaIIlJag/EVm2A5XRxwL7JptfzTBwkwi4Fj6mMEYCcHXnTkObIlhVjhIoxX1kvau0JB5HQcQ3F3M6uNmASMVIq5m+7g9aOtLS6jtNeXjZHwGGLxGPLzAgjk5uDwm8l+5uexeQHSPlUlReLXgM3k4UM4cUqoYCp4ETRMI+1HhhxA242fR89VH4CeV2CrpbiB/FMHUfOHHyGr/+wMgLYVTXlv16Zb0bX1Y0jkF9v5g6km0HoItb//ATxNL8qsKsdEsBr9sZ+F+DDjiZM5xMEcYxD8yEgYHo9HUNU7oQEnpaRZRUyewOag/xkDGOlqRh8LfCU7e5qvJtF2wz/j7E13QM8pkINj3pcqaejiK3HiH+9lZC2zU8B0Ac8qt2PbbWi95QtIBAhOt+8VR3j5Why/4/uI0r/VZCKdx3mBO4sVMtMwBUsq1oOmoycwNjpKYC4JMBwOz1iGOF+ayM0WVYhGQInTGOofxzBVkVLhOyfC+eDQRWvRteWjNjBzapoD82+0ajHar/006p+4G4Z2LsgIfxtb1IiOa2+HVHPGzHsTxUVo3/ll1H/vU+lqISWpnVJdtKKOHSuP4Zu/vAdv95XCn+1AIhY50dnZuWt65TmrBVXV8glVIS+NNCFEcONxhXnPeS6HkZ79a24gVxTMVvHIg5M+tHwTfawS6iQrCmr3v+9amF7nVHCTD1ozvHoTIouW2QHISEl5Z9FFhIIqzgO+eNMw7th8Cnmh451tra0f1XW9bbZSbxaArG9FHtVp8dhphEbsgCJaJsybzFER5j9Yc61GUQTl5CCeXy6DyWRZPl7ZcP6JSQcC05+FBCNq5l5LJHX68zjDSILzxUCI7etMLM01h8mtY+erZc9frsRaOMJhuV4iQ70M95Ytx0g5UwQPa/5qeWJiMixkKplQOPOUTKbLPYW/YhyxtPwUFU1clzpXTa99LQwghXJCiGVBT6TFssxvaW0pZlLTo3CNDc+9JCHmJJ6SuUzO/qQI6g73zX2vVOmQacNKyzzpHewnlZoyVtFEqZJcMEDLUsbFSheSITsSabNehIKTL83FARnCfN0tUq5NUTI8Ck68NLf1eHlW5xl4O95mbnVPkbOadk7eykolhYSqzE748w1vWCznTSgbt60+wMhpielLU6y46Rn42lqlMp+51pFOqH9+VAoBTLKgwQEXnHgROaeOEsV5RsVW+fzDcI4M2Mk+bVbhj65J+l3En2CUXmSdfz1ptpNBkRYyh8yJggtCUcRjE5LKGRmh1vwGvJxpOVAx0S67FFENHTW7f4Sit/ZJNSMsLioH0RRRnOhxNPz6LvhbTs64Vxhj8R8fRsnB3fa9kzUsI3AmHYuQEBPRdhzBlHVeEs08oia6BgfP5aUCFhUeJ3kg1AcTrsWkqpBywhK+3hasun8XgpfeiNDSy6Vlff1tKH7jTwhQxhnUqlK1MCz3X3ydlGMFJw6w+opQ0nVj5UP/guDa7RhuWA/D7YF3sBPFh/ZSzbyJpKg4MrwUoV0sWFHg5+dPWsYn05JJdM7hJTOPrjj6AlHptE4R6gXAAoqablGyKw6YoyPQ8gvtaE6QzkgYi154hO1RGRBEKSSineH0TKiWM9s/i87rb5X0KDy4D8t+8TXpl4K+lfsfl00EInsyVPmbpSfPWY4aV/SrGgm5lJg5QiG58tc615LnTNQqegeHEExGbAsKLKwuYI6NQs3y2upeJCNZyYvky3mgCDdoHQHYoKQy04FBJOloSTV6NnzAlmN0x8GLt2JkyWqqIV3qVHkvm7Cu/BT3RyKw+HvGggoBijX+XJ+FwkK72Bc/BYOScS0XBNBUMcDSqGto0L5CYd8rG0XaGIPqsJOtERqCMdwPMzrOmRbBJ2k/1bKmbJsI1RKmpDOys5BeApW8EQpHqhMhwQRQ0Qf9W0ycMTQAYzQ0ZRVAzeL9PFdC6+UXpNd8+Mi+PoS6dbRfEEXHmE+7Qmjq6cW60sV2NLxsDfCLJ2OMWnEoHtZ4BCtmGaLJPTHSK702Yy/KpnNXPIq4WLxz2pVCJsJGckuQCvYglSm/MpMzfQ1HnBNUFnsHY2HUX2UvUIlgLiQkW5tTRdf5VJE6x5rrfop1+ws7W0ZltbSGMzfYDy03Tz50yoA4pcKKtjXY6K+imRxJ3qtPw93BOCCKGRpCjcZR8MpTDIh2ZJbgMqAyLQOaf6sswxRG7GxXEo2N9mmRC/upFc724w2miNQFWVAcvPe1o8cxYpL2ZI9cNdxGVh19YBBOFpdKES0QjcKKReTK18wZsqfJZJj3dZ7C8v+4GeNL10if83Qx+bcfp695Zy5ETYzMAYW/ayxrtGQcyY4O1K8Gysttaorc/A49b1jHsylrTq0xuwWJqf3tVrze3oZryiqA4+8AdZy9LRtNHHz1HWjePjjyimD4/TBFZBUWFNYTAlGIY1EUywVWU0ZE12AXCvrSYl+stol1HSttNc1eHFV4XmEWVwhOswxGX+ZNKiFjJIRG6vrtO+x5EPlPeEZzC2NMCi8DFwhQ0F2EipYQHt93ANfcfpt9TizefvhDQOMK4Hd7xjHKOtFK8geXl36ZBSWLmp6fYtfHSm+GmKo6dR9Nsa2lWXZCF2lByeyKikDDuK/QbxWmD6fYOGWX23cCG9fZXiEYLax3ku5zqhN7mOf7Lxhg5ug1sfup5/CVa7eiQeTBAZFzON7aOgU5jU44mSk21+gY6o2gszuCkfAgRoP2vqfJwlSIWGEVKNpU35LNlBFWWFokb4/TBMmAXD6npB646CL6SLcTXREN9St0eNymrByEseNx8R4B9HASP0zNU83MCTBuYexkEF++/6d4etcuqCIViclO2vUnvBzQmksU+NyWnNnRMbtFqC7CYR2j6eWFaMx2U3GNGCAZCIoh+GgdIbvyKCR8/JQAcxglaSkXr2ndD3S0pp9n2tQU9+7dw1jQgntbdRydr+Kad/PrrIE9vz+If2f5d8+Om+0B+WgBL2kSSdj7EaKoFwMQA2Z9O+tbE/MdmaApmuhT7OGLzyyneJYlgYmNpD0E9/w+/LYtibsMa/5+5wUoOmlO4t7f/QWR3iC+c931yF5FH1y+2MQzhzUM01q53nOvjJjmX781KNxgLKagN6ygutRCeaGF7m7gmb3A4bfwP8ei+NJQavb674IBZjZjTidxv9mCg+09uHvNKmxfutxAwKvh9TMO1JQl5XswGZDTxMz8hfuk9CfACYq+2qoiwgC22JvEH3YDL7+OI82D+BbjwpPjxsL7XvD+rIj4pxNo6kpiR+cr2FjQZH2irEDf3Nyt1j7HgdRWWvDRh4SiYlEgByl8Zj6g8oWhlL3dH6NOpfJD36CCV+ldSlAPPnPYeu2dEH7RncIfoxaiF8qGC96AjtBCzToOKGyBMSuvzG2sbH4baxgs11TloyaQjXyXGwEG0GyGcw+zhiasIlYF0lsS9jsG6azAiBhn4o5Q/Izy76GOELojMetIEsahPh1vEXdn5K+g/bveYReGCZmsVmI4wD9Fw6mI1H5eU74jZAuzZW5s9RcXPaAXLLJRiU2SSAiRjjMPn0zgnvRyb4TAow6FBrTmX8f6fwE42yFq/RyWgZVO1GUxfdFiLr+G1Q6xC5sYtk2YUqClxpHtxOIGDRvodyII93QncGzUwNn3Ety7Bphlq3QhRtxpjeJd5MWKXA078/1ZV3uKSpe584qhUnaZMqdPrxT4tbZhW0DXtxlC2ukxlIWDI4nRkddGYskn22PYH02h07SLgST/S0Tw7ix7QQDFVNe6sX55Mb7BAriEfuWJ2kWDNzzuqvbVX46ssmo4GG2UBSRDL51viEWn2PpSCxflOvTotUV9Z6+tTbSPMad2ZnmoWhUkYzFET3TghdMR3DWYhP43A0gh49+0PPfRr3/3A3WlOa3QIi+jh2q3nxLuqadTeMdTDqc/AFOsvk2UfvY7FKa9oiINKaoBgd/lUlDC8nzEOSI2TqA7fMhlSXTrB+GvrUXjklL7hQRaE3ufxuXf/Ak6CPDBCxmzeiEXFyu4eeendtRVVLZAG90v43s5K+yyAvHihVOsp1KBpCYtbZrogR+vYDEGGHMc1HmCrTWsKysqhFKxJOC8vDyUlpbCQ30W0R0yFy6ttEW1tDR94sYbgJVLsMs9+yLlXw9QvBl2WZ33jvXvo5ru/ovtEGkziRw2PJ4FhydrwtdctFcHcrEXS9GECuy16lhjZuNDNxn46lch286d6RcpUibzZxZKSoqhK34MDU/a5bPsTRxPgCA3Y2WZhqv/JgDLHVi7+bLkOr/zhRmbJoNDHINJgHQai1YSljuLAMHVcWwOuSQQ4RQ9h4uglbGA1UwpCHawvrvlFjt7mETqoOB0+nPQP2BvrkxZ2uf3TRuAVaW43aO8xwDFujLH9fHNVyQ1qccmR0N+HSDAmJkHzaHKd5iGGGdfQRVJmTxnZsXEmOnGR35Wi5Ntronz111H6q3kBIlSiNLHwZqyZ0CVJdeUg12VLQbWrsRWJtia9xQg6Zm7th7b65badJkMUKfLdffSCp5CWRSLLZpxEtTHEdURqh8JNj39WpOJwbAXJzvcU4K+WGeRzBbWZwQOhj0YHpl9prdciZwKF7a/pwCLHbjsisuwRLyeOT0ZjTJB9fQpcOfmQbzgLvbUKzBGgsbQgnwC1UnP9GZCXMOW2iC2roxMPFqURMeOpXfmxMp9tg+jCS96+9KvAkyjqahkllVhp3uBr9qqC6FnhR8716zGDN8T9BwK04KDPpkeMi/7iJfx1qMLeVRhfbSfeDUPCRVXlQfxq13tyM+3Z0nQ8rHHgCNHbHFub/Q4EE7mobOLkTkxDSDZksOo/b4GrMlWUfueAGRMLlm1BNurquwZnHxnTKxMddCKyWK4OfNCsShyHhQcpA+2E6I0uaFgY1U//vfzZ1FSanNbLBqJl3o2MHBUV6dfVCcDHNSqak4B3mlTMDQ6ezVy6SXILnfigwsB+H8CDAACcMlhCn8awgAAAABJRU5ErkJggg=='
image=EMOJI_BASE64_HAPPY_IDEA
sg.set_global_icon(micon)

def welcome():
    print('''
    Hi, and welcome to the quotes application that allows you to
    give yourself inspiring and uplifting messages from time to time.
    It works by uploading a file with quotes, names or verbs.
    Name files go with verbs so that random messages can be generated in the format
    "God loves me," or "My friend cares for me."
    ''')


def take_files():
    '''
    Ask for the input files
    and return a list of the files
    '''
    print("""
    Which would you be uploading?
    Please select your choice:
    [Q]uotes, [N]ames and [A]ctions or [B]oth quotes and names/actions""")

    choices = ['Q','N','A','B']
    phrases = ['quotes', 'names', 'actions']
    choice = input('>> ')
    phrase = []
    try:
        if choice.upper() == 'B':
            phrase = phrases
        elif choice.upper() == 'N' or choice.upper() == 'A':
            phrase.extend(phrases[1:])
        elif choice.upper() == 'Q':
            phrase.append(phrases[0])
        else:
            print("You have not made a valid choice")
        test_dict = make_dict_from_file(phrase)
        return test_dict
    except Exception as e:
        print(e)

def make_dict_from_file(phrases):
    my_dict = {}
    i = 0
    n = len(phrases)
    try:
        while i < n:
            infile = check_file_exists(phrases[i])
            my_dict[phrases[i].capitalize()] = make_list(infile)
            i += 1
        return my_dict
    except Exception as e:
        print("This was the error we got", e)
        quit()


def make_list(filename):
    """
    Take a multiline file separated by commas and returns a list of the contents of the file.
    Works with comma separated entries on multiple lines.
    """
    with open(filename, 'r') as infile:
        lst = [line.rstrip().split(',') for line in infile]
        lst = [name.lstrip() for names in lst for name in names if len(name)]
        return lst


def check_file_exists(phrase):
    filename = input(f'Enter the name of the {phrase} file: ')
    filepath = Path(os.path.expanduser(filename))
    try:
        if not filepath.exists():
            print(f"The file {filename} was not found. Please try again.")
            filepath = check_file_exists(phrase)
        return filepath
    except KeyboardInterrupt:
        quit()

def make_csv(filepath, my_dict):
    for key, values in my_dict.items():
        with open(f'{filepath}/app_{key.lower()}.csv', 'w') as csv_writer:
            csv_writer.write(key + '\n')
            for value in values:
                csv_writer.write(value + '\n')


def choose_quotes(my_dict):
    obj = random.choice(('you', 'me'))

    def chosen_names():
        random_name = random.choice(my_dict["Names"])
        random_act = random.choice(my_dict["Actions"])
        chosen = f"{random_name} {random_act} {obj}."
        return chosen

    def chosen_quotes():
        chosen = random.choice(my_dict["Quotes"])
        return chosen

    if 'Names' not in my_dict.keys():
        quote = chosen_quotes()
    elif 'Quotes' not in my_dict.keys():
        quote = chosen_names()
    else:
        sample = random.randrange(2)
        if sample == 1:
            quote = chosen_names()
        else:
            quote = chosen_quotes()
    return quote

# 2. Save dictionary to json file rather than csv
def save_quotes(quotesfile, my_dict):
    with open(quotesfile,'w+') as qf:
        json.dump(my_dict, qf, indent=4, sort_keys=True)

# 2b. Load in the dictionary from json
def read_quotes(quotesfile):
    with open(quotesfile, 'r') as qf:
        my_dict = json.load(qf)
    return my_dict

def update_quotes(quotesfile):
    my_dict = read_quotes(quotesfile)
    new_dict = take_files()
    for key, value in new_dict.items():
        if (key, value) in my_dict.items():
            continue
        else:
            my_dict[key] = value
        save_quotes(quotesfile, my_dict)
    return my_dict

def show_quotes(my_dict):
    def_num, def_freq = (10, 5)
    num_times = input("How many times do you want to run it? ") 
    frequency_mins = input("How long between each quote? Enter time in minutes: ")
    if not num_times: 
        num_times = def_num
    else:
        num_times = float(num_times)
    if not frequency_mins: 
        frequency = def_freq
    else:
        frequency = float(frequency_mins)*60
    i = 1
    try:
        while i <= num_times:
            my_quote = choose_quotes(my_dict)
            s.run(["notify-send", "-i", "face-cool","Hope", my_quote])
            if i == num_times:
                break
            else:
                time.sleep(frequency)
                i += 1
    except KeyboardInterrupt:
        quit()  

def take_files_gui():
    phrases = ['quotes', 'names', 'actions']
    phrase = []
    def welcome():
        eventa, _ = sg.Window('QuotesInspire',
                       [[sg.T('Welcome to the quotes application that allows you to give yourself inspiring and uplifting messages from time to time.')],
                        [sg.T('Using it is so easy! Simply upload a file that has quotes you want to use. Ready to start?''')],
                        [sg.B('Yes please'), sg.B('Not now')]]).read(close=True)
        return eventa

    event = welcome()     

    while True:
        if event in (sg.TIMEOUT_EVENT, sg.WIN_CLOSED, None, 'Cancel'):
            break
        if event == 'Not now':
            choice, _ = sg.Window('Continue?', [[sg.T('Sure you want to leave?')], [sg.Yes('Yes', s=10), sg.No('No', s=10)]], disable_close=True).read(close=True)
            if choice == 'Yes':
                sg.popup_auto_close('Alright then, see you soon!')
                break
            elif choice == 'No':
                    event = welcome()
            elif event == 'Yes please':
                event, _ = sg.Window('QuotesInspire',
                              [[sg.T('Which would you be uploading?')],
                              [sg.T('Please select your choice below')],
                              [sg.B('Quotes'), sg.B('Names-Actions'), sg.B('Both')]]).read(close=True)
                if event in (sg.TIMEOUT_EVENT, sg.WIN_CLOSED, None, 'Cancel'):
                    quit()
                elif event == 'Names-Actions':
                    phrase.extend(phrases[1:])
                elif event == 'Both':
                    phrase = phrases
                elif event == 'Quotes':
                    phrase.append(phrases[0])
                test_dict = make_dict_from_file_gui(phrase)
                return test_dict
               

def make_dict_from_file_gui(phrases):
    my_dict = {}
    i = 0
    n = len(phrases)
    try:
        while i < n:
            infile = sg.popup_get_file(f'Choose your {phrases[i]} file', title='QuotesInspire', keep_on_top=True)
            my_dict[phrases[i].capitalize()] = make_list(infile)
            i += 1
            return my_dict
    except Exception as e:
        print("This was the error we got", e)
        quit()


def show_quotes_gui(my_dict):
    def_num, def_freq = (10, 5)
    num_times, frequency_mins = choice_gui()
    if not num_times: 
        num_times = def_num
    else:
        num_times = float(num_times)
    if not frequency_mins: 
        frequency = def_freq
    else:
        frequency = float(frequency_mins)*60
    i = 1
    try:
        while i <= num_times:
            my_quote = choose_quotes(my_dict)
            # s.run(["notify-send", "-i", "face-cool","Hope", my_quote])
            event, _ = sg.Window('QuotesInspire', [[sg.T(my_quote), sg.Image(data=image)]],
            element_justification='r', auto_close=True, auto_size_text=False).read(close=True)
            if i == num_times:
                break
            else:
                time.sleep(frequency)
                i += 1
    except KeyboardInterrupt:
        quit()


def choice_gui():
    layout = [[sg.T('How many times to run?', size= (50,1)), sg.In(key='-num_times-', size=(10,1))],
          [sg.T('How long between each quote? Enter time in minutes.', size=(50,1)), sg.In(key='-freq-', size=(10,1))],
          [sg.Submit('Okay'), sg.B('Cancel')]]
    event, values = sg.Window('QuotesInspire', layout).read(close=True)
    number, frequency = values.values()
    return number, frequency          

def run_again_gui(quotesfile):
    event, _ = sg.Window('QuotesInspire', [[sg.T('Welcome back! What would you like to do?')],
                       [sg.B('Run'), sg.B('Update'), sg.B('Reset'), sg.B('Cancel')]]).read(close=True)
    if event in (sg.TIMEOUT_EVENT, sg.WIN_CLOSED, None, 'Cancel'):
        quit()
    if event == 'Run':
        my_dict = read_quotes(quotesfile)
    elif event == 'Update':
        my_dict = update_quotes(quotesfile)
    elif event == 'Reset':
        my_dict = take_files_gui()
    return my_dict
     
def run_again(quotesfile):
    user_input = input("""Would you like to Run [R], Update [U] or Reset [S]? """)
    if user_input == 'Run' or user_input.upper() == 'R':
        my_dict = read_quotes(quotesfile)
    elif user_input == 'Update' or user_input.upper() == 'U':
        my_dict = update_quotes(quotesfile)
    elif user_input == 'Reset' or user_input.upper() == 'S':
        my_dict = take_files()
    return my_dict
    