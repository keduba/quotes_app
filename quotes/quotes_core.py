#! /usr/bin/python3

from quotes_helpers import *

# Main App directory
p = Path.cwd()
APP_DIR = Path(p.home(),'.quotesapp').joinpath('data')
APP_DIR.mkdir(parents=True, exist_ok=True)
QUOTESFILE = os.path.join(APP_DIR, 'quotesfile.json')

def main():
    welcome()
    filepath_exists = Path(aquotesfile).exists()
    if filepath_exists:
        app_dict = run_again(aquotesfile)
    else:
        app_dict = take_files()
        save_quotes(aquotesfile, app_dict)
    show_quotes(app_dict)

def main_gui():
    filepath_exists = Path(aquotesfile).exists()
    if filepath_exists:
        app_dict = run_again_gui(aquotesfile)
    else:
        app_dict = take_files_gui()
        save_quotes(aquotesfile, app_dict)
    show_quotes_gui(app_dict)     


if __name__ == '__main__':
    aquotesfile = QUOTESFILE

    if len(sys.argv) > 1 and sys.argv[1] == 'gui':
        main_gui()
    elif len(sys.argv) == 1 or (len(sys.argv) > 1 and sys.argv[1] == 'cli'):
        main()
