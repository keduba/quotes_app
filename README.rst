Quotes App Module
=================

This is a project for running personal quotes at intervals on your own computer.

It takes a text or csv file of quotes, or names and actions and makes a quotation of them. Then prints it to the screen by making a random choice of one of them.

The project repo can be found at https://gitlab.com/keduba/quotes_app

