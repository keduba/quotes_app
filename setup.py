# -*- coding: utf-8 -*-

# Learn more: https://gitlab.com/keduba/quotes_app.git

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='quotes',
    version='0.1.0',
    description='An open-source quotes application',
    long_description=readme,
    author='Keduba',
    author_email='jfk.uba@proton.me',
    url='https://gitlab.com/keduba/quotes_app',
    license=license,
    packages=['quotes'],
    install_requires=['PySimpleGUI']
)

